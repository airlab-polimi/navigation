# README
---
If you need to use this repo, please clone also **common_pkgs** (https://bitbucket.org/airlab-polimi/common_pkgs) repo in the same workspace. It contains packages that are shared with other repositories of the project.
**IMPORTANT:** if you are planning to run ROAMFREE on a ordinary pc, it's **STRONGLY** recommended to compile it in _Release_ mode (following the steps below). Otherwise the working rate will be too low.
---

* In order to compile, run 
```shell
$ chmod +x __tmp_install_script/*
$ ./__tmp_install_script/compile_dependencies_install.bash
$ ./__tmp_install_script/runtime_dependencies_install.bash
$ catkin_make --pkg roamfree -DCMAKE_BUILD_TYPE=Release
$ catkin_make
```

---
In order to visualize your data, run:
```shell
$ roslaunch husky_viz view_robot.launch
```
