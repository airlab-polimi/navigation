#include <ros/ros.h>
#include <husky_msgs/WheelsEncoder.h>
#include <nav_msgs/Odometry.h>

ros::Publisher enc_pub;


float R; // wheel radius
float L; // wheels separation
float mulR = 1.0;
float mulL = 1.0;

void odom_to_encoder_Callback(const nav_msgs::Odometry& msg)
{
  ROS_INFO("subscribed to husky odom - start conversion");

  // w is the angular velocity, v is the linear velocity
  float w = msg.twist.twist.angular.z;
  float v = msg.twist.twist.linear.x;

  // v_r and v_l are the linear velocity of the 2 set of wheels
  float v_r = v + w*L*mulL/2;
  float v_l = v - w*L*mulL/2;

  // convert linear velocity to wheels rotation velocity: w_l w_r
  float w_r = v_r / R;
  float w_l = v_l / R;

  // ROS_INFO_STREAM("w_r: " << w_r << " w_l: " << w_l << " L:" << L*mulL << " w: " << w << " v: " << v);

  // create the msgs to publish
  husky_msgs::WheelsEncoder enc_msg;

  enc_msg.stamp = msg.header.stamp;
  enc_msg.r_encoder = w_r;
  enc_msg.l_encoder = w_l; 

  // publish msgs
  enc_pub.publish(enc_msg);


}



int main(int argc, char **argv)
{
  ros::init(argc, argv, "odom2encoder");

  double delay = 1.0;

  ros::NodeHandle nh;

  if (!nh.getParam("wheels_R", R)) {
    ROS_FATAL_STREAM("wheel radius not set");
  }

  if (!nh.getParam("wheels_L", L)) {
    ROS_FATAL_STREAM("wheel separation not set");
  }

  nh.getParam("wheel_separation_multiplier", mulL);

  nh.getParam("wheel_radius_multiplier", mulR);

  enc_pub = nh.advertise<husky_msgs::WheelsEncoder>("/husky_wheels_encoder", 1);

  ros::Subscriber odom_sub = nh.subscribe("/husky_velocity_controller/odom", 1, odom_to_encoder_Callback);


  // Sleep for the parameterized amount of time, to give
  // other nodes time to start up (not always necessary)
  ros::Duration start_delay(delay);
  start_delay.sleep();

  ros::spin();

  return 0;
}