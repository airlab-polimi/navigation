var searchData=
[
  ['eigen_5fmake_5faligned_5foperator_5fnew',['EIGEN_MAKE_ALIGNED_OPERATOR_NEW',['../class_r_o_a_mimu_1_1_i_m_u_integral_handler.html#a00134fafaba755a25e302b7a07bad18f',1,'ROAMimu::IMUIntegralHandler']]],
  ['enums_2eh',['Enums.h',['../_enums_8h.html',1,'']]],
  ['estimate',['estimate',['../class_r_o_a_mestimation_1_1_factor_graph_filter.html#add2486c7c7aac41f2b4af56d8bcd076d',1,'ROAMestimation::FactorGraphFilter::estimate(int nIterations)=0'],['../class_r_o_a_mestimation_1_1_factor_graph_filter.html#a41dc920fa87cdd8c233c681f626a886e',1,'ROAMestimation::FactorGraphFilter::estimate(PoseVertexWrapperVector poses, int nIterations)=0']]],
  ['euclidean1d',['Euclidean1D',['../namespace_r_o_a_mestimation.html#a9b6b69f715f8b8dc0de2228ca5446926aa1e6176093292b8543ba4ef757b3cc63',1,'ROAMestimation']]],
  ['euclidean1dprior',['Euclidean1DPrior',['../namespace_r_o_a_mestimation.html#a2d1422ca38c02ae876d73248255010a4ae8a2b3af1b9208c7f9a425858e70a1f1',1,'ROAMestimation']]],
  ['euclidean2d',['Euclidean2D',['../namespace_r_o_a_mestimation.html#a9b6b69f715f8b8dc0de2228ca5446926a9b38c9292036eefc6c5f43288e61a982',1,'ROAMestimation']]],
  ['euclidean3d',['Euclidean3D',['../namespace_r_o_a_mestimation.html#a9b6b69f715f8b8dc0de2228ca5446926a6d9191d19d5925f82b665e26d5e24777',1,'ROAMestimation']]],
  ['euclidean3dprior',['Euclidean3DPrior',['../namespace_r_o_a_mestimation.html#a2d1422ca38c02ae876d73248255010a4ac58bc449949217a77540f61c8591729b',1,'ROAMestimation']]]
];
