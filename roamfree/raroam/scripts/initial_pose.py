#!/usr/bin/env python

import rospy
import tf

from math import sin, cos, atan2

from sensor_msgs.msg import MagneticField
from geometry_msgs.msg import PoseWithCovarianceStamped

ZMag = [0.0, 0.0, 0.0]
ZGPS = [0.0, 0.0, 0.0]
Ngps = 0
Nmag = 0


def gps_cb(msg):
    global ZGPS
    global Ngps
    Ngps += 1
    ZGPS[0] = (ZGPS[0] * (Ngps - 1) + msg.pose.pose.position.x) / Ngps
    ZGPS[1] = (ZGPS[1] * (Ngps - 1) + msg.pose.pose.position.y) / Ngps
    ZGPS[2] = (ZGPS[2] * (Ngps - 1) + msg.pose.pose.position.z) / Ngps


def mag_cb(msg):
    global ZMag
    global Nmag
    Nmag += 1
    ZMag[0] = (ZMag[0] * (Nmag - 1) + msg.magnetic_field.x) / Nmag
    ZMag[1] = (ZMag[1] * (Nmag - 1) + msg.magnetic_field.y) / Nmag
    ZMag[2] = (ZMag[2] * (Nmag - 1) + msg.magnetic_field.z) / Nmag


def run():
    rospy.init_node('firstPose', anonymous=True)

    # wait for the transformation from /roamfree to /gps is available
    r = rospy.Rate(10.0)
    tf_listener = tf.TransformListener()

    while not rospy.is_shutdown():
        try:
            (GPS_trans, GPS_rot) = tf_listener.lookupTransform('/base_link', '/gps', rospy.Time(0))
            break

        except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
            pass

        r.sleep()
        continue

    # subscribe to topics

    subenu = rospy.Subscriber('/enu', PoseWithCovarianceStamped, gps_cb)
    submag = rospy.Subscriber('/magnetic', MagneticField, mag_cb)

    # sleep for a minute, collect messages
    collection_time = 60
    collection_time = rospy.get_param('~collection_time', collection_time)
    rospy.sleep(collection_time)

    # subenu.unregister()
    # submag.unregister()

    pub = rospy.Publisher('/initialPose', PoseWithCovarianceStamped, queue_size=1)

    r = rospy.Rate(1)

    theta = atan2(ZMag[0], ZMag[1])

    msg = PoseWithCovarianceStamped()

    msg.header.stamp = rospy.Time.now()

    msg.pose.pose.position.x = ZGPS[0] - GPS_trans[0] * cos(theta) + GPS_trans[1] * sin(theta)
    msg.pose.pose.position.y = ZGPS[1] - GPS_trans[1] * cos(theta) - GPS_trans[0] * sin(theta)
    msg.pose.pose.position.z = -GPS_trans[2] + ZGPS[2]

    msg.pose.pose.orientation.x = 0.0
    msg.pose.pose.orientation.y = 0.0
    msg.pose.pose.orientation.z = sin(theta / 2.0)
    msg.pose.pose.orientation.w = cos(theta / 2.0)

    rospy.loginfo("publishing initial pose")
    while not rospy.is_shutdown():
        pub.publish(msg)
        r.sleep()

if __name__ == '__main__':
    try:
        run()
    except rospy.ROSInterruptException:
        pass
