#include "raroam/ImuHandler.h"
#include "ROAMestimation/StochasticProcessFactory.h"

#include <ros/ros.h>
#include <iostream>

using namespace ROAMestimation;

namespace raroamfree
{

ImuHandler::ImuHandler(FactorGraphFilter* filter, int imu_N, double imu_dt,
			bool isAccBiasFixed, bool isGyroBiasFixed) :
			filter(filter), imu_N(imu_N), imu_dt(imu_dt),
			isAccBiasFixed(isAccBiasFixed), accBias(3),
			isGyroBiasFixed(isGyroBiasFixed), gyroBias(3), T_OS_IMU(7), x0(7)
{
	initialized = false;

	//imu centric by default
	T_OS_IMU << 0.0, 0.0, 0.0, 0.5, 0.5, -0.5, 0.5;
	x0 << 0.0, -1.0, 0.0, 0.5, -0.5, 0.5, -0.5;

	//zero bias by default
	accBias << 0.0, 0.0, 0.0;
	gyroBias << 0.0, 0.0, 0.0;

	// -- accelerometer bias
	//ParameterWrapper_Ptr ba_par = StochasticProcessFactory::addEucl3DRandomConstant(filter, "IMUintegralDeltaP_Ba", accBias);
	ParameterWrapper_Ptr ba_par = filter->addConstantParameter(Euclidean3D, "IMUintegralDeltaP_Ba", accBias, true);
	// -- gyroscope bias
	//ParameterWrapper_Ptr bw_par = StochasticProcessFactory::addEucl3DRandomConstant(filter,"IMUintegralDeltaP_Bw", gyroBias);
	ParameterWrapper_Ptr bw_par = filter->addConstantParameter(Euclidean3D, "IMUintegralDeltaP_Bw", gyroBias, true);
	//setup roamfree imu integral handler
	//imu = new ROAMimu::IMUIntegralHandler(imu_N, imu_dt); // instantiate the new handler
	//imu = new ROAMimu::IMUIntegralHandler(filter, "imu", imu_N ,imu_dt, ba_par, bw_par, T_OS_IMU);
	imu = new ROAMimu::IMUIntegralHandler(filter, "IMUintegral", imu_N ,imu_dt, ba_par, bw_par, T_OS_IMU);
	imu->getSensorNoises() = 1e-4 * Eigen::Matrix<double, 6, 6>::Identity(); // init the sensor noises
}

void ImuHandler::addMeasurement(double za[3], double zw[3], double t)
{
	if (!initialized)
		initialize(t);

	imu->step(za, zw);

}

void ImuHandler::initialize(double t)
{
	imu->init(true,t, x0);

	initialized = true;
}

}
