
#include "raroam/OdomImuGps_RaROAM.h"
#include <fstream>
#include <Eigen/Dense>

using namespace ROAMestimation;

namespace raroamfree {

bool poseFixed = false;

double encoder_l = 0.0;

ros::Publisher gyro_calibrated;

Eigen::MatrixXd odomCov(6, 6); // covariance of Odometry readings
Eigen::MatrixXd gyroscopeCov(3, 3); // covariance of Gyroscope readings
Eigen::MatrixXd magnetometerCov(2, 2); // covariance of Magnetometer readings
Eigen::MatrixXd accelerometerCov(3, 3); // covariance of Accelerometer readings
Eigen::MatrixXd gpsCov(3,3); // covariance of Gps readings


OdomImuGps_RaROAM::OdomImuGps_RaROAM() :
				config(),
				filter(NULL)
				//imuHandler(NULL)
{

//----------------------------------------
// INIT SENSORS
//----------------------------------------
	//setup roamfree
	ROS_INFO("Init ROAMFREE");
	initRoamfree();	


	//----------------------------------------
	//	CHOOSE THE METHOD FOR GETTING  AN INITIAL POSITION:
	//	AUTOMATIC OR MANUAL
	//----------------------------------------

	if(config.getInitialPoseByTopic) {
	// 1) automatic initial position
		init_done = false;
		ROS_INFO("SUBSCRIBE to initialposeTopic");
		init_sub = n.subscribe("/initialPose", 30, &OdomImuGps_RaROAM::init_Cb, this);
		ros::Rate rate(1);
		while(!init_done){
			ros::spinOnce();
			rate.sleep();
		}

	} else {
		// manual initial position
		init_done = true;
		// wait for the bag to start
		while(ros::Time::now().toSec() == 0);
		
		t0 = ros::Time::now().toSec();
	}
	ROS_INFO_STREAM("x0: " << config.x0 << " fine x");
	ROS_INFO_STREAM("t0: " << t0 );

	filter->setInitialPose(config.x0, t0);

	ROS_INFO("SUBSCRIBE to encodersTopic");
	encoder_sub = n.subscribe("/husky_wheels_encoder", 1, &OdomImuGps_RaROAM::encoder_Cb, this);

	//setup the odom
	//ROS_INFO("Init ODOM");
	//initOdom();

	//setup the odom
	ROS_INFO("Init DIFF DRIVE ODOM");
	initDiffDriveOdom();

	int i=0;
	ros::Rate testRate(1);
	while(i < 5){
		ros::spinOnce();
		testRate.sleep();
		i++;
	}

	if(config.enabledSensors.gps){
		ROS_INFO("SUBSCRIBE to gpsTopic");
		gps_sub = n.subscribe("/fix", 1, &OdomImuGps_RaROAM::gpsCb, this);
		//setup the gps
		ROS_INFO("Init GPS");
		initGps();
	}
		
	if(config.enabledSensors.accelerometer || config.enabledSensors.gyroscope){
		ROS_INFO("SUBSCRIBE to imuTopic");
		imu_sub = n.subscribe("/imu/data", 1, &OdomImuGps_RaROAM::imuCb, this);
	}

	if(config.enabledSensors.magnetometer) {
		ROS_INFO("SUBSCRIBE to magneticTopic");
		mag_sub = n.subscribe("/imu/mag", 1, &OdomImuGps_RaROAM::magCb, this);
	}
	//setup the imu
	ROS_INFO("Init IMU");
	initIMU();

}

//----------------------------------------
//----------------------------------------

OdomImuGps_RaROAM::~OdomImuGps_RaROAM() {
	if (filter)
		delete filter;

	//	if (imuHandler)
	//		delete imuHandler;

}

void OdomImuGps_RaROAM::publishOdom() {

	ROS_INFO("Get the estimate");

	// the oldest
	ROAMestimation::PoseVertexWrapper_Ptr newest_pose = filter->getNewestPose();
	const Eigen::VectorXd &x = newest_pose->getEstimate();

	// publish the pose as a tf transform from global_frame to base_link_frame_id
	tf::Transform tf_transform;
	tf_transform.setOrigin(tf::Vector3(x(0), x(1), x(2)));
	tf_transform.setRotation(tf::Quaternion(x(4), x(5), x(6), x(3)));
	tf_broadcaster.sendTransform(
			tf::StampedTransform(tf_transform,
					ros::Time(newest_pose->getTimestamp()), "odom", config.trackedFrame));

	// publish the pose as a tf transform from base_link_frame_id to global_frame
	// tf::Transform tf_transform_inverse;
	// tf_transform_inverse = tf_transform.inverse();
	// tf_broadcaster.sendTransform(
	// 		tf::StampedTransform(tf_transform_inverse,
	// 				ros::Time(newest_pose->getTimestamp()), "base_link", "odom"));

	// // publish base_link -> base_footprint tf
	// tf::Transform tf_transform2;
	// tf_transform2.setOrigin(tf::Vector3(0.0, 0.0, -(x(2) + 0.145))); //0.145 is the standard height of base_link
	// tf::Matrix3x3 m(tf::Quaternion(x(4), x(5), x(6), x(3)));
	// double roll, pitch, yaw;
	// m.getRPY(roll, pitch, yaw);
	// // double yaw_angle = tf::getYaw(tf::Quaternion(x(4), x(5), x(6), x(3)));
	// tf::Quaternion q;
	// q.setRPY(-roll, -pitch, 0.0);
	// tf_transform2.setRotation(q);
	// tf_broadcaster.sendTransform(
	// 		tf::StampedTransform(tf_transform2,
	// 				ros::Time(newest_pose->getTimestamp()), "base_link", "base_footprint2"));


	// publish the pose as a Pose
	// geometry_msgs::PoseStamped pose_msg;

	// pose_msg.header.seq = 0.0; //TODO: output a sequence counter
	// pose_msg.header.stamp = ros::Time(newest_pose->getTimestamp());
	// pose_msg.header.frame_id = solver->global_frame_;

	// pose_msg.pose.position.x = x(0);
	// pose_msg.pose.position.y = x(1);
	// pose_msg.pose.position.z = x(2);

	// pose_msg.pose.orientation.x = x(4); // in roamfree w is the first component of unit quaternions
	// pose_msg.pose.orientation.y = x(5);
	// pose_msg.pose.orientation.z = x(6);
	// pose_msg.pose.orientation.w = x(3);

	// pose_pub.publish(pose_msg);

	// publish the pose as an Odometry
	nav_msgs::Odometry odom_msg;

	odom_msg.header.seq = 0.0; //TODO: output a sequence counter
	odom_msg.header.stamp = ros::Time(newest_pose->getTimestamp());
	// odom_msg.header.frame_id = "odom";
	odom_msg.header.frame_id = "odom";

	odom_msg.pose.pose.position.x = x(0);
	odom_msg.pose.pose.position.y = x(1);
	odom_msg.pose.pose.position.z = x(2);

	odom_msg.pose.pose.orientation.x = x(4); // in roamfree w is the first component of unit quaternions
	odom_msg.pose.pose.orientation.y = x(5);
	odom_msg.pose.pose.orientation.z = x(6);
	odom_msg.pose.pose.orientation.w = x(3);

	// Since the twist is not specified it's all set to zero	

	odom_pub = n.advertise<nav_msgs::Odometry>("/odometry/roamfree", 10);

	odom_pub.publish(odom_msg);

}



void OdomImuGps_RaROAM::init_Cb(const geometry_msgs::PoseWithCovarianceStamped& msg) {
	if (init_done)
		return;
	ROS_INFO("INIT_CB");
	t0 = msg.header.stamp.toSec();
	config.x0.resize(7);
    config.x0 << msg.pose.pose.position.x, msg.pose.pose.position.y, 0.0,
			msg.pose.pose.orientation.w, msg.pose.pose.orientation.x, msg.pose.pose.orientation.y, msg.pose.pose.orientation.z;
	ROS_INFO("Got initial pose...");
	init_done = true;
}


void OdomImuGps_RaROAM::odomCb(const nav_msgs::Odometry& msg) {
	ROS_INFO("odom callback");
	double t_odom = msg.header.stamp.toSec();

	Eigen::VectorXd z_Odom(6);
	z_Odom << msg.twist.twist.linear.x, 0, 0, 0, 0, msg.twist.twist.angular.z;
	filter->addSequentialMeasurement("WheelsOdometry",t_odom, z_Odom, odomCov);
}

void OdomImuGps_RaROAM::encoder_Cb(const husky_msgs::WheelsEncoder &msg){
	ROS_INFO("encoder callback");
	double t_odom = msg.stamp.toSec();

	Eigen::VectorXd z_Odom(2);
	z_Odom <<  -(double)msg.l_encoder,(double)msg.r_encoder;
	ROS_INFO("PRIMA add seq measuremnet");	
	filter->addSequentialMeasurement("DifferentialOdometry",t_odom, z_Odom, odomCov);
	ROS_INFO("DOPO add seq measuremnet");
}

// void OdomImu_RaROAM::imuCalibCb(const sensor_msgs::Imu& msg) {

// 	std::cout<<"imu gyro callback"<<std::endl;

// 	t_imu+= 1.0/imuRate;
// 	// double t = msg.header.stamp.toSec();

// 	Eigen::VectorXd eig_zw(3);
// 	// fill temporaries with measurements
// 	//eig_za << msg.x, msg.y, 9.8;
// 	//double z = (msg.angular_velocity.z)*0.3066036023887353e-3;
// 	//double z = (msg.angular_velocity.z)*0.412371e-3;
// 	double z = msg.angular_velocity.z*2.98054e-4;

// 	eig_zw << 0, 0, z ;//+ 0.025803887886194;

// 	geometry_msgs::Vector3 msg_new;

// 	sum += z;
// 	numDati++;
// 	if (numDati == 500){
// 		biasStimato = sum/(numDati);
// 	}

// 	//publish to debug calibration
// 	msg_new.x = numDati;
// 	msg_new.y = biasStimato;
// 	msg_new.z = z;

// 	gyro_calibrated.publish(msg_new);

// 	//std::cout<<"adding acc"<<std::endl;

// 	//filter->addSequentialMeasurement("Accelerometer", t_imu, eig_za, accelerometerCov);

// 	std::cout<<"adding gyro"<<std::endl;

// 	filter->addSequentialMeasurement("Gyroscope", t_imu, eig_zw, gyroscopeCov);

// 	//filter->handleDeferredMeasurements();
// 	//imuHandler->addMeasurement(eig_za.data(), eig_zw.data(), t);
// }

void OdomImuGps_RaROAM::imuCb(const sensor_msgs::Imu& msg) {
	ROS_INFO("imu callback");
	double t_imu = msg.header.stamp.toSec();

	// Eigen::VectorXd z_Mag(3);
	// // // z_Mag << msg.orientation.x, msg.orientation.y, msg.orientation.z, msg.orientation.w;	
	// z_Mag << 0.0, 0.0, msg.orientation.w;
	// z_Mag += config.mag_generated_bias;
	// filter->addSequentialMeasurement("Magnetometer", t_imu, z_Mag, magnetometerCov);

	Eigen::VectorXd z_Gyro(3);
	z_Gyro << msg.angular_velocity.x, msg.angular_velocity.y, msg.angular_velocity.z;
	// z_Gyro << 0.0, 0.0, msg.angular_velocity.z;
	// z_Gyro += config.gyro_generated_bias;
	filter->addSequentialMeasurement("Gyroscope", t_imu, z_Gyro, gyroscopeCov);

	Eigen::VectorXd z_Acc(3);
	// z_Acc << msg.linear_acceleration.x, msg.linear_acceleration.y, msg.linear_acceleration.z;
	z_Acc << msg.linear_acceleration.x, 0.0, 9.81;
	// Eigen::VectorXd z_Acc(1);
	// z_Acc << msg.linear_acceleration.x;
	// z_Acc += config.acc_generated_bias;
	filter->addSequentialMeasurement("Accelerometer", t_imu, z_Acc, accelerometerCov);
}


void OdomImuGps_RaROAM::magCb(const sensor_msgs::MagneticField& msg) {
	ROS_INFO("magnetometer callback");
	double t_mag = msg.header.stamp.toSec();

	Eigen::VectorXd z_Mag(3);
	z_Mag << msg.magnetic_field.x, msg.magnetic_field.y, msg.magnetic_field.z;
	filter->addSequentialMeasurement("Magnetometer", t_mag, z_Mag, magnetometerCov);
}

void OdomImuGps_RaROAM::gpsCb(const geometry_msgs::PoseWithCovarianceStamped& msg) {
	ROS_INFO("gps callback");
	double t_gps = msg.header.stamp.toSec();

	Eigen::VectorXd z_Gps(3);
	// z_Gps << msg.pose.pose.position.x ,msg.pose.pose.position.y, msg.pose.pose.position.z;
	z_Gps << msg.pose.pose.position.x ,msg.pose.pose.position.y, msg.pose.pose.position.z;

	filter->addSequentialMeasurement("GPS",t_gps, z_Gps, gpsCov);

	// publish enu gps in frame map
	geometry_msgs::PointStamped enuP_msg;

	enuP_msg.header.stamp = msg.header.stamp;
	enuP_msg.header.frame_id = "odom";

	enuP_msg.point.x = msg.pose.pose.position.x;
	enuP_msg.point.y = msg.pose.pose.position.y;
	enuP_msg.point.z = msg.pose.pose.position.z;

	enu_pub = n.advertise<geometry_msgs::PointStamped>("/enu/points", 10);

	enu_pub.publish(enuP_msg);
}


void OdomImuGps_RaROAM::initRoamfree() {

	filter = FactorGraphFilterFactory::getNewFactorGraphFilter();
	filter->setLowLevelLogging(true); // default log folder
	filter->setWriteGraph(true);
	system("mkdir -p /tmp/roamfree/");
	system("rm -f /tmp/roamfree/*.log");
	// filter->setDeadReckoning(true);
	filter->setDeadReckoning(false);

	if (config.useGaussNewton) {
		filter->setSolverMethod(GaussNewton);
	} else {
		filter->setSolverMethod(LevenbergMarquardt);
	}

}

void OdomImuGps_RaROAM::initOdom() {
	filter->addSensor("WheelsOdometry",GenericOdometer , true, true);
	filter->setSensorFrame("WheelsOdometry", config.sensorsTransformation.odom);
	filter->setRobustKernel("WheelsOdometry", true, 0.5);
	// odomCov = 0.01 * Eigen::MatrixXd::Identity(6, 6); //sim
	odomCov = 0.1 * Eigen::MatrixXd::Identity(6, 6); //real
	odomCov(3,3) = 1;
	odomCov(4,4) = 1;
	// odomCov(5,5) = 0.01; //sim
	odomCov(5,5) = 10; //real


	// Eigen::VectorXd r(1);
	// r << 0.165;
	// Eigen::VectorXd l(1);
	// l << 0.550;
	// filter->addConstantParameter(Euclidean1D, "WheelsOdometry_R", r, true);
	// filter->addConstantParameter(Euclidean1D, "WheelsOdometry_L", l, true);
}

void OdomImuGps_RaROAM::initDiffDriveOdom() {

	filter->addSensor("DifferentialOdometry",DifferentialDriveOdometer , true, true);
	filter->setSensorFrame("DifferentialOdometry", config.sensorsTransformation.odom);
	filter->setRobustKernel("DifferentialOdometry", true, 0.5);


	odomCov = 0.02 * Eigen::MatrixXd::Identity(6, 6); //real
	odomCov(3,3) = 10;
	odomCov(4,4) = 10;
	// odomCov(5,5) = 0.01; //sim
	odomCov(5,5) = 0.5; //real

	Eigen::VectorXd r(1);
	r << config.odomParam.wheels_radius;
	Eigen::VectorXd l(1);
	l << config.odomParam.wheels_separation * config.odomParam.wheel_separation_multiplier;
	filter->addConstantParameter(Euclidean1D, "DifferentialOdometry_R", r, true);
	filter->addConstantParameter(Euclidean1D, "DifferentialOdometry_L", l, true);



}

void OdomImuGps_RaROAM::initIMU() {
	//setup the handlers
	//	imuHandler = new ImuHandler(filter, config.imu_N, config.imu_dt,
	//			config.isAccBiasFixed, config.isGyroBiasFixed);
	//
	//	imuHandler->setSensorframe(config.T_O_IMU, config.x0);


	/* ---------------------- Configure sensors ---------------------- */

	bool isbafixed = true, isbwfixed = true, isbzfixed = true;

	// Gyroscope sensor
	if(config.enabledSensors.gyroscope) {
		filter->addSensor("Gyroscope", AngularVelocity, false, true); // master sensor, sequential sensor
		filter->setSensorFrame("Gyroscope", config.sensorsTransformation.imu);
		// filter->setRobustKernel("Gyroscope", true, 1.65);
		// Initial gain and bias calibration parameters for gyroscope
		// gain
		Eigen::VectorXd gyroGain0(3);
		gyroGain0 << 1.0, 1.0, 1.0;
		filter->addConstantParameter(Euclidean3D, "Gyroscope_G", gyroGain0, true);
		// bias
		Eigen::VectorXd gyroBias0(3);
		gyroBias0 << 0.0, 0.0, 0.0;
		// ParameterWrapper_Ptr bw_par = filter->addLinearlyInterpolatedParameter(Euclidean3D,
		// 		"Gyroscope_B", gyroBias0, isbwfixed, 1.0);
		filter->addConstantParameter(Euclidean3D, "Gyroscope_B", gyroBias0, true);

		// Cov
		// gyroscopeCov = 0.01 * Eigen::MatrixXd::Identity(3, 3); //sim 
		gyroscopeCov = 0.1 * Eigen::MatrixXd::Identity(3, 3); //real
	}

	if(config.enabledSensors.accelerometer) {
		// Accelerometer sensor
		filter->addSensor("Accelerometer", LinearAcceleration, false, true); 
		filter->setSensorFrame("Accelerometer", config.sensorsTransformation.imu);
		// filter->setRobustKernel("Accelerometer", true, 1.65);
		// filter->shareSensorFrame("Gyroscope", "Accelerometer");
		// Initial gain and bias calibration parameters for accelerometer
		// gain
		Eigen::VectorXd accGain0(3);
		accGain0 << 1.0, 1.0, 1.0;
		filter->addConstantParameter(Euclidean3D, "Accelerometer_G", accGain0, true);
		// bias
		Eigen::VectorXd accBias0(3);
		accBias0 << 0.0, 0.0, 0.0;
		// ParameterWrapper_Ptr ba_par = filter->addLinearlyInterpolatedParameter(Euclidean3D,
		// 		"Accelerometer_B", accBias0, isbafixed, 1.0); 	// it is not fixed and it is time varying
		// ba_par->setProcessModelType(RandomWalk);
		// ba_par->setRandomWalkNoiseCov(10*Eigen::MatrixXd::Identity(3, 3));
		filter->addConstantParameter(Euclidean3D, "Accelerometer_B", accBias0, true);
		// Cov
		// accelerometerCov = 0.1 * Eigen::MatrixXd::Identity(3, 3); //sim
		accelerometerCov = 0.5 * Eigen::MatrixXd::Identity(3, 3); //real
	}

	if(config.enabledSensors.magnetometer) {
		// // Magnetometer sensor
		filter->addSensor("Magnetometer", VectorField, false, true);
		filter->setSensorFrame("Magnetometer", config.sensorsTransformation.imu);
		// filter->setRobustKernel("Magnetometer", true, 1.65);
		// Initial h, R and S calibration parameters for magnetometer
		filter->addConstantParameter(Euclidean3D, "Magnetometer_h", config.magnetometerParam.h0, true);
		// R
		filter->addConstantParameter(Matrix3D, "Magnetometer_R", config.magnetometerParam.r0, true);
		// S
		filter->addConstantParameter(Euclidean3D, "Magnetometer_S", config.magnetometerParam.s0, true);
		// Cov
		magnetometerCov = 0.01 * Eigen::MatrixXd::Identity(3, 3); //real
	}

	// // Magnetometer sensor
	// filter->addSensor("Magnetometer", VectorFieldAsCompass, false, true);
	// filter->setSensorFrame("Magnetometer", R_OS_IMU);
	// // filter->setRobustKernel("Magnetometer", true, 1.65);
	// // Initial h, R and S calibration parameters for magnetometer
	// // h
	// Eigen::VectorXd magh0(3);
	// // magh0 << -0.06, 0.44, -0.58;
	// magh0 << 0.25, 0.00, -0.38;
	// filter->addConstantParameter(Euclidean3D, "Magnetometer_h", magh0, true);
	// // Cov
	// magnetometerCov = 0.1 * Eigen::MatrixXd::Identity(2, 2); //sim
	// // magnetometerCov = 0.01 * Eigen::MatrixXd::Identity(3, 3); //real

	/* ---------------------- Initialize imu handler---------------------- */

	// Eigen::VectorXd x0(7);
	// x0 << 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0;
	// double tt0 = ros::Time::now().toSec();
	// tt0 = (tt0 == 0.0) ? 0.1 : tt0;
	// filter->setInitialPose(x0, tt0);

	//	if (config.accBias.size() == 3L && config.gyroBias.size() == 3L)
	//	{
	//		imuHandler->setBias(config.accBias, config.gyroBias);
	//	} else {
	//		imuHandler->setBias(config.acc_generated_bias, config.gyro_generated_bias);
	//	}
}

void OdomImuGps_RaROAM::initGps() {
	filter->addSensor("GPS",AbsolutePosition , false, true);
	filter->setSensorFrame("GPS", config.sensorsTransformation.gps);
	// filter->setRobustKernel("GPS", true, 4);
	filter->setRobustKernel("GPS", false, 4);
	gpsCov = 0.01 * Eigen::MatrixXd::Identity(3, 3); //sim
	// gpsCov = 0.1 * Eigen::MatrixXd::Identity(3, 3);//real
	// gpsCov = 0.01 * Eigen::MatrixXd::Identity(3, 3);//eurecat
	// gpsCov(0,0) = 20; //eurecat
}

void OdomImuGps_RaROAM::run() {

	ros::Rate rate(20);

	while (ros::ok()) {

		ros::spinOnce();

		if (!poseFixed){
			std::cout<<"set fixed 1"<<std::endl;
			filter->getNthOldestPose(0)->setFixed(true);

			// if (filter->getNthOldestPose(1)){
			// 	std::cout<<"set 2"<<std::endl;
			// 	filter->getNthOldestPose(1)->setEstimate(filter->getNthOldestPose(0)->getEstimate());
			// 	std::cout<<"set fixed 2"<<std::endl;
			// 	filter->getNthOldestPose(1)->setFixed(true);
			// 	poseFixed = false;
			// }
			poseFixed = true;
		}

		if (filter->getNthOldestPose(1)) {
			ROS_INFO("ESTIMATE!");
			bool ret = filter->estimate(config.iterationN);
			filter->marginalizeOldNodes(config.poseWindowLength);
			publishOdom();
		}

		rate.sleep();

	};

}

}

int main(int argc, char *argv[]) {

	ros::init(argc, argv, "raroam_node");

	ROS_INFO("initialization done");
	try
	{
		raroamfree::OdomImuGps_RaROAM node;
		ROS_INFO("Localization node started");
		node.run();
		ROS_INFO("Localization node shut down");

	}
	catch (std::runtime_error& e)
	{
		ROS_INFO("runtime error");

		return -1;
	}

	return 0;
}
