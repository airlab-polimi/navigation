
#include "raroam/RaROAM_Config.h"

#include <vector>

using namespace std;

namespace raroamfree {

RaROAM_Config::RaROAM_Config() : n("~")
{
  try
  {
    initROS();
    initIMU();
    initOdom();
    initGPS();
    //initCamera(n);
    initRoamfree();
  }
  catch (runtime_error& e)
  {
    ROS_FATAL_STREAM(e.what());
    throw e;
  }
  ROS_INFO("config end");
}

void RaROAM_Config::initROS()
{
  n.param<string>("trackedFrame", trackedFrame, "base_link");

  if (!n.getParam("gps_frame", sensorFrames.gps)) 
    throw runtime_error("No GPS frame specified");

  if (!n.getParam("imu_frame", sensorFrames.imu)) 
    throw runtime_error("No IMU frame specified");

  if (!n.getParam("odom_frame", sensorFrames.odom)) 
    throw runtime_error("No odom frame specified");


}


void RaROAM_Config::initIMU() {

  n.param<bool>("magnetometer_enabled", enabledSensors.magnetometer, false);
  n.param<bool>("accelerometer_enabled", enabledSensors.accelerometer, false);
  n.param<bool>("gyroscope_enabled", enabledSensors.gyroscope, false);
  
  if(enabledSensors.magnetometer || enabledSensors.gyroscope || enabledSensors.accelerometer) {
    ROS_INFO("IMU is enabled");
    tf::StampedTransform transform;
    listener.waitForTransform("/"+trackedFrame, "/"+sensorFrames.imu, ros::Time(0), ros::Duration(5));
    try{
      listener.lookupTransform("/"+trackedFrame, "/"+sensorFrames.imu, ros::Time(0), transform);
    }
    catch (tf::TransformException ex){
      ROS_ERROR("%s",ex.what());
      throw runtime_error("No IMU frame detected");
    }
    sensorsTransformation.imu.resize(7);
    sensorsTransformation.imu << transform.getOrigin().x(), transform.getOrigin().y(), transform.getOrigin().z(),
                                 transform.getRotation().w(), transform.getRotation().x(), transform.getRotation().y(), transform.getRotation().z();

  }

  if(enabledSensors.magnetometer){
    ROS_INFO("Magnetometer is enabled");
    //Setup magnetometer
    vector<double> h0_std(3);
    if (n.getParam("magnetometer/h", h0_std) && h0_std.size() == 3) {
      magnetometerParam.h0.resize(3);
      magnetometerParam.h0 << h0_std[0], h0_std[1], h0_std[2];
    } else {
      throw runtime_error("No h0 detected for magnetometer");
    }

    vector<double> r0_std(9);
    magnetometerParam.r0.resize(9);
    if (n.getParam("magnetometer/r", r0_std) && r0_std.size() == 9) {
      magnetometerParam.r0 << r0_std[0], h0_std[1], h0_std[2], r0_std[3], h0_std[4], h0_std[5], r0_std[6], h0_std[7], h0_std[8];
    } else {
      magnetometerParam.r0 << 1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0;
      ROS_INFO_STREAM("Using default value for r0 of magnetometer");
    }

    vector<double> s0_std(3);
    magnetometerParam.s0.resize(3);
    if (n.getParam("magnetometer/s", s0_std) && s0_std.size() == 3) {
      magnetometerParam.s0 << s0_std[0], s0_std[1], s0_std[2];
    } else {
      magnetometerParam.s0.setZero();
      ROS_INFO_STREAM("Using default value for r0 of magnetometer");
    }
  ROS_INFO("Magnetometer is ready");
  } else {
    ROS_INFO("Magnetometer is not enabled");
  }

  if(enabledSensors.gyroscope) {
    ROS_INFO("Gyroscope enabled");
    //Setup gyroscope
    n.param<bool>("isGyroBiasFixed", isGyroBiasFixed, true);
    ROS_INFO_STREAM(
        "Gyroscope bias" << (isGyroBiasFixed ? "" : " not") << " fixed");

    vector<double> gyroBias_std(3);
    gyroBias.resize(3);
    if (n.getParam("gyroBias", gyroBias_std) && gyroBias_std.size() == 3) {
      gyroBias << gyroBias_std[0], gyroBias_std[1], gyroBias_std[2];
    } else {
      gyroBias.setZero();

      ROS_INFO("No default gyroscope bias");
    }

    if (n.getParam("gyroStd", gyro_stdev)) {
      ROS_INFO_STREAM("Adding noise on gyroscope with std " << gyro_stdev);
    } else {
      gyro_stdev = 0.0;

      ROS_INFO("No added noise on angular velocity");
    }

    double gen_gyro_bias_stdev;
    gyro_generated_bias.resize(3);
    gyro_generated_bias.setZero();

    if (n.getParam("genGyroBiasStd", gen_gyro_bias_stdev))
    {
      ROS_INFO_STREAM("Generating random bias on gyroscope with std: " << gyro_generated_bias.transpose());
    }
    else
    {
      ROS_INFO("No added bias on angular velocity");
    }
  } else {
    ROS_INFO("Gyroscope not enabled");
  }

  
  if(enabledSensors.accelerometer) {  
    ROS_INFO("Accelerometer is enabled");
    //Setup accelerometer
    n.param<bool>("isAccBiasFixed", isAccBiasFixed, true);
    ROS_INFO_STREAM("Accelerometer bias" << (isAccBiasFixed ? "" : " not") << " fixed");

    vector<double> accBias_std(3);
    accBias.resize(3);
    if (n.getParam("accBias", accBias_std) && accBias_std.size() == 3) {
      accBias << accBias_std[0], accBias_std[1], accBias_std[2];
    } else {
      accBias.setZero();

      ROS_INFO("No default accelerometer bias");
    }

    if (n.getParam("accStd", acc_stdev)) {
      ROS_INFO_STREAM("Adding noise on accelerometer with std " << acc_stdev);
    } else {
      acc_stdev = 0.0;

      ROS_INFO("No added noise on acceleration");
    }

    double gen_acc_bias_stdev;
    acc_generated_bias.resize(3);
    acc_generated_bias.setZero();

    if (n.getParam("genAccBiasStd", gen_acc_bias_stdev))
    {

      ROS_INFO_STREAM(
          "Generating random bias on accelerometer with std: " << acc_generated_bias.transpose());
    }
    else
    {
      ROS_INFO("No added bias on acceleration");
    }
  } else {
    ROS_INFO("Accelerometer is not enabled");
  }


  // //Setup imu integration steps
  // if (!n.getParam("imu_N", imu_N)) {
  //   throw runtime_error("parameter imu_N undefined");
  // }

  // //Setup imu nominal period
  // if (!n.getParam("imu_dt", imu_dt)) {
  //   throw runtime_error("parameter imu_dt undefined");
  // }
}

void RaROAM_Config::initGPS() {

  n.param<bool>("gps_enabled", enabledSensors.gps, false);
  if(enabledSensors.gps) {
    ROS_INFO("GPS is enabled");
    tf::StampedTransform transform;
    try{
      listener.lookupTransform("/"+trackedFrame, "/"+sensorFrames.gps, ros::Time(0), transform);
    }
    catch (tf::TransformException ex){
      throw runtime_error("No GPS frame detected");
    }
    sensorsTransformation.gps.resize(7);
    sensorsTransformation.gps << transform.getOrigin().x(), transform.getOrigin().y(), transform.getOrigin().z(),
                                 transform.getRotation().w(), transform.getRotation().x(), transform.getRotation().y(), transform.getRotation().z();

  } else {
    ROS_INFO("GPS is not enabled");
  }
}


void RaROAM_Config::initOdom() {

  tf::StampedTransform transform;
  try{
    listener.lookupTransform("/"+trackedFrame, "/"+sensorFrames.odom, ros::Time(0), transform);
  }
  catch (tf::TransformException ex){
    throw runtime_error("No odom frame detected");
  }
  sensorsTransformation.odom.resize(7);
  sensorsTransformation.odom << transform.getOrigin().x(), transform.getOrigin().y(), transform.getOrigin().z(),
                               transform.getRotation().w(), transform.getRotation().x(), transform.getRotation().y(), transform.getRotation().z();


  if (!n.getParam("wheels_radius", odomParam.wheels_radius)) {
    throw runtime_error("RAROAM: Wheels radius not set");
  }

  if (!n.getParam("wheels_separation", odomParam.wheels_separation)) {
    throw runtime_error("RAROAM: Wheels separation not set");
  }
  
  if (!n.getParam("wheel_separation_multiplier", odomParam.wheel_separation_multiplier)) {
    throw runtime_error("RAROAM: Wheels separation multiplier not set");
  }

}



void RaROAM_Config::initRoamfree() {


  n.param<bool>("pose_by_topic", getInitialPoseByTopic, true); 
  if(!getInitialPoseByTopic){
    //Init initial pose
    vector<double> x0_std(7);
    if (!n.getParam("initialPose", x0_std) || x0_std.size() != 7) {
      throw runtime_error("Incorrect initial pose or no initial pose specified");
    }
    x0.resize(7);
    x0 << x0_std[0], x0_std[1], x0_std[2], x0_std[6], x0_std[3], x0_std[4], x0_std[5];

  }

  n.param<bool>("useGaussNewton", useGaussNewton, true);

  ROS_INFO_STREAM(
      "Using " << (useGaussNewton ? "Gauss-Newton" : "Levenberg–Marquardt") << " Optimization algorithm");

  if (!n.getParam("iterationN", iterationN)) {
    n.param<int>("iterationN", iterationN, 1);
  }
  ROS_INFO_STREAM("Using " << iterationN << " iterations for each optimization");

  if (!n.getParam("poseWindowLength", poseWindowLength)) {
    n.param<double>("poseWindowLength", poseWindowLength, 5.0);
  }
  ROS_INFO_STREAM("pose window length " << poseWindowLength << "s");

}

}
