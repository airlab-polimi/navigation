
#include "raroam/RaROAM_Imu.h"
#include <fstream>
#include <Eigen/Dense>

//#include <visualization_msgs/Marker.h>

//#include "NoiseGeneration.h"

using namespace ROAMestimation;

namespace raroamfree {
  double imuRate = 100; // Hz rate of the IMU readings
  const static int _OFF = -1;
  double t_imu = 0;

  // parameters of the IMU biases
  double ba_x = 0.0; //biases on the accelerometer measurements
  double ba_y = 0.0;
  double ba_z = 0.0;

  double ba_dx = 0.0; // increments on the accelerometer biases in m/s^3
  double ba_dy = 0.0;
  double ba_dz = 0.0;

  double ba_fx = 0.00; // frequency of the sinusoidal bias on the accelerometer
  double ba_fy = 0.000;
  double ba_fz = 0.0000;

  double ba_Ax = 0.000; // amplitude of the sinusoidal bias on the accelerometer
  double ba_Ay = 0.0000;
  double ba_Az = 0.0000;

  double bw_x = 0.0; // biases on the gyroscope measurements
  double bw_y = 0.0;
  double bw_z = 0.0;

  double bw_dx = 0.00; // increments on the gyroscope biases
  double bw_dy = 0.00;
  double bw_dz = 0.00;

  double bw_fx = 0.0; // frequency of the sinusoidal bias on the gyroscope
  double bw_fy = 0.00;
  double bw_fz = 0.000;

  double bw_Ax = 0.000; // amplitude of the sinusoidal bias on the gyroscope
  double bw_Ay = 0.000;
  double bw_Az = 0.000;

  Eigen::MatrixXd gyroscopeCov(3, 3); // covariance of Gyroscope readings
  Eigen::MatrixXd accelerometerCov(3, 3); // covariance of Accelerometer readings

  
  

RaROAM_Imu::RaROAM_Imu(RaROAM_Config& config) :
    config(config), 
    filter(NULL),
    imuHandler(NULL) {
  //setup roamfree
  ROS_INFO("Init ROAMFREE");
  initRoamfree();
  ROS_INFO("Init IMU");
  //setup the imu handler
  initIMU();
  ROS_INFO("SUBSCRIBE to imuTopic with vec");
  //subscribe to sensor topics
  imu_vec_sub = n.subscribe("/r2p/imu", 60000, &RaROAM_Imu::imuCbVec, this);
  //ROS_INFO("SUBSCRIBE to imuTopic");
  //imu_sub = n.subscribe(config.imuTopic, 60000, &RaROAM_Imu::imuCb, this);
  ROS_INFO("SUBSCRIBE to odomTopic");
  odom_sub = n.subscribe("/odom", 60000, &RaROAM_Imu::odomCb, this);

  //advertise markers topic
  //markers_pub = n.advertise<visualization_msgs::Marker>(
  //    "/visualization/features", 1);
}

RaROAM_Imu::~RaROAM_Imu() {
  if (filter)
    delete filter;

  if (imuHandler)
    delete imuHandler;

}

void RaROAM_Imu::publishPose() {
  std::cout<<"get newest pose"<<std::endl;

  ROAMestimation::PoseVertexWrapper_Ptr cameraPose_ptr =
      filter->getNewestPose();
      std::cout<<"camera get estimate"<<std::endl;
  if (cameraPose_ptr)
  {
    std::cout<<"ok"<<std::endl;
  }
  else
  {
    std::cout<<"null"<<std::endl;
  }
  const Eigen::VectorXd &camera = cameraPose_ptr->getEstimate();

  tf::Transform T_WR_tf(
      tf::Quaternion(camera(4), camera(5), camera(6), camera(3)),
      tf::Vector3(camera(0), camera(1), camera(2)));
std::cout<<"send transform"<<std::endl;
  br.sendTransform(
      tf::StampedTransform(T_WR_tf, ros::Time(cameraPose_ptr->getTimestamp()),
          "odom", config.trackedFrame));

}

void RaROAM_Imu::imuCb(const sensor_msgs::Imu& msg) {
  //ROS_INFO("imu callback");
  std::cout<<"imu callback"<<std::endl;
  double t = msg.header.stamp.toSec();

  Eigen::VectorXd eig_za(3), eig_zw(3);

  // fill temporaries with measurements
  eig_za << msg.linear_acceleration.x, msg.linear_acceleration.y, msg.linear_acceleration.z;
  eig_zw << msg.angular_velocity.x, msg.angular_velocity.y, msg.angular_velocity.z;

  eig_za += config.acc_generated_bias;
  eig_zw += config.gyro_generated_bias;

  //imuHandler->addMeasurement(eig_za.data(), eig_zw.data(), t);
}

void RaROAM_Imu::odomCb(const nav_msgs::Odometry& msg) {
  //ROS_INFO("imu callback");
  std::cout<<"odom callback"<<std::endl;
  t_imu = msg.header.stamp.toSec();;
}

void RaROAM_Imu::imuCbVec(const geometry_msgs::Vector3& msg) {
  t_imu+= 1.0/imuRate;
// double t = msg.header.stamp.toSec();
//Using fake imu mesurament

  Eigen::VectorXd eig_za(3), eig_zw(3);

  // fill temporaries with measurements
  eig_za << msg.x, msg.y, 9.81;
  eig_zw << 0, 0, msg.z;

  filter->addSequentialMeasurement("Accelerometer", t_imu, eig_za, accelerometerCov);
  filter->addSequentialMeasurement("Gyroscope", t_imu, eig_zw, gyroscopeCov);

  //imuHandler->addMeasurement(eig_za.data(), eig_zw.data(), t);

  
}

void RaROAM_Imu::initRoamfree() {
  filter = FactorGraphFilterFactory::getNewFactorGraphFilter();
  filter->setLowLevelLogging(true); // default log folder
  filter->setWriteGraph(true);
  system("mkdir -p /tmp/roamfree/");
  system("rm -f /tmp/roamfree/*.log");
  filter->setDeadReckoning(false);

  if (config.useGaussNewton) {
    filter->setSolverMethod(GaussNewton);
  } else {
    filter->setSolverMethod(LevenbergMarquardt);
  }
}

void RaROAM_Imu::initIMU() 
{
  //setup the handlers
  imuHandler = new ImuHandler(filter, config.imu_N, config.imu_dt,
      config.isAccBiasFixed, config.isGyroBiasFixed);

  imuHandler->setSensorframe(config.T_O_IMU, config.x0);
 
 /* ---------------------- Configure sensors ---------------------- */

  bool isbafixed = true, isbwfixed = true;

  // Accelerometer sensor
  Eigen::VectorXd R_OS_ACC(7); // Transformation between Accelerometer and robot frame
  R_OS_ACC << 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0;

  filter->addSensor("Accelerometer", LinearAcceleration, true, true); // master sensor, sequential sensor
  filter->setSensorFrame("Accelerometer", R_OS_ACC);

  //gain calibration parameter
  Eigen::VectorXd accGain0(3); // Initial gain and bias calibration parameters for accelerometer
  accGain0 << 1.0, 1.0, 1.0;

  filter->addConstantParameter(Euclidean3D, "Accelerometer_G", accGain0, true);

  //bias calibration parameter
  Eigen::VectorXd accBias0(3);
  accBias0 << 0.0, 0.0, 0.0;

  // it is not fixed and it is time varying
  ParameterWrapper_Ptr ba_par = filter->addLinearlyInterpolatedParameter(Euclidean3D,
      "Accelerometer_B", accBias0, isbafixed, 1.0);

  ba_par->setProcessModelType(RandomWalk);
  ba_par->setRandomWalkNoiseCov(10*Eigen::MatrixXd::Identity(3, 3));

  Eigen::MatrixXd accelerometerCov(3, 3); // covariance of Accelerometer readings
  accelerometerCov = 0.0016 * Eigen::MatrixXd::Identity(3, 3);

  // Gyroscope sensor

  filter->addSensor("Gyroscope", AngularVelocity, false, true);
  filter->shareSensorFrame("Accelerometer", "Gyroscope"); // usually gyro and acc are housed together

 //gain calibration parameter
  Eigen::VectorXd gyroGain0(3);
  gyroGain0 << 1.0, 1.0, 1.0;

  filter->addConstantParameter(Euclidean3D, "Gyroscope_G", gyroGain0, true);

  //bias calibration parameter
  Eigen::VectorXd gyroBias0(3);
  gyroBias0 << 0.0, 0.0, 0.0;

  Eigen::Matrix3d gyroBias0Cov = 1e-4 * Eigen::MatrixXd::Identity(3, 3);

  /*
   f->addConstantParameter(Euclidean3D, "Gyroscope_B", gyroBias0, true);
   f->addPriorOnConstantParameter(Euclidean3DPrior, "Gyroscope_B", gyroBias0, gyroBias0Cov);
   //*/

  //
  ParameterWrapper_Ptr bw_par = filter->addLinearlyInterpolatedParameter(Euclidean3D,
      "Gyroscope_B", gyroBias0, isbwfixed, 1.0);
  gyroscopeCov = 1.15172e-05 * Eigen::MatrixXd::Identity(3, 3);
  accelerometerCov = 0.0016 * Eigen::MatrixXd::Identity(3, 3);

  /* ---------------------- Initialize ---------------------- */

  Eigen::VectorXd x0(7);

  filter->setInitialPose(x0, t_imu);

  if (config.accBias.size() == 3 && config.gyroBias.size() == 3)
  {
    imuHandler->setBias(config.accBias, config.gyroBias);
  } else {
    imuHandler->setBias(config.acc_generated_bias, config.gyro_generated_bias);
  }
}

void RaROAM_Imu::run() {
 // ros::NodeHandle n("~");

  ros::Rate rate(5);

  while (ros::ok()) {
    rate.sleep();

    ros::spinOnce();
    //ROS_INFO("SPIN!");
    std::cout<<"spin!"<<std::endl;
    if (filter->getWindowLenght() > config.minWindowLenghtSecond)
    {
      filter->getNthOldestPose(0)->setFixed(true);
      filter->getNthOldestPose(1)->setEstimate(filter->getNthOldestPose(0)->getEstimate());
      filter->getNthOldestPose(1)->setFixed(true);
      std::cout<<"estimate!"<<std::endl;
  
      //ROS_INFO("Run estimation");
      ROS_INFO("ESTIMATE!");
      bool ret = filter->estimate(config.iterationN);
    }

    if (filter->getOldestPose()) {
       ROS_INFO("publish pose!");
      //publishPose();
    }
  };

}

}

int main(int argc, char *argv[]) {
  


  ros::init(argc, argv, "raroam_test_node");

  ros::NodeHandle n0;

  ROS_INFO("initialization done");
  try
  {
      ROS_INFO("Loading config");

      raroamfree::RaROAM_Config config;

      ROS_INFO("Configuration loaded");

      raroamfree::RaROAM_Imu n(config);

      ROS_INFO("Localization node started");

      n.run();

      ROS_INFO("Localization node shut down");

    }
    catch (std::runtime_error& e)
    {
          ROS_INFO("runtime error");

      return -1;
    }

    return 0;
}