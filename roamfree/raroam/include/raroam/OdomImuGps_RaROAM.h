/*
 * c_slam_roamfree,
 *
 *
 * Copyright (C) 2014 Davide Tateo
 * Versione 1.0
 *
 * This file is part of c_slam_roamfree.
 *
 * c_slam_roamfree is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * c_slam_roamfree is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with c_slam_roamfree.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OdomImuGps_RaROAM_H_
#define OdomImuGps_RaROAM_H_

#include <ros/ros.h>
#include <sensor_msgs/Imu.h>
#include <std_msgs/Float32.h>
#include <husky_msgs/WheelsEncoder.h>
#include <geometry_msgs/Vector3.h>
#include <nav_msgs/Odometry.h>
#include <sensor_msgs/MagneticField.h>
#include <geometry_msgs/PoseWithCovarianceStamped.h>
#include <geometry_msgs/Vector3Stamped.h>
#include <geometry_msgs/PointStamped.h>
#include <message_filters/subscriber.h>
#include <message_filters/time_synchronizer.h>
#include <tf/tf.h>
#include <tf/transform_broadcaster.h>

#include <string>
#include <unistd.h>

#include "raroam/RaROAM_Config.h"
#include "raroam/ImuHandler.h"

namespace raroamfree
{
	const static int _OFF = -1;

class OdomImuGps_RaROAM
{
	public:
		OdomImuGps_RaROAM();
		~OdomImuGps_RaROAM();
		void run();


	protected:
		void imuCb(const sensor_msgs::Imu &msg);
		// void magCb(const geometry_msgs::Vector3Stamped& msg);
		void magCb(const sensor_msgs::MagneticField& msg);
		void odomCb(const nav_msgs::Odometry &msg);
		void gpsCb(const geometry_msgs::PoseWithCovarianceStamped &msg);
		void encoder_Cb(const husky_msgs::WheelsEncoder &msg);
		void publishOdom();
		void waitForOdometry(const std::string &topic);
		void tmpCallback(const nav_msgs::Odometry &msg);
		void init_Cb(const geometry_msgs::PoseWithCovarianceStamped &msg);
		// void initPosition();

	private:
		void initIMU();
		void initOdom();
		void initDiffDriveOdom();
		void initGps();
		void initRoamfree();
		

	protected:
		RaROAM_Config config;

	protected:
		ros::NodeHandle n;
		ros::Publisher odom_pub;
		ros::Publisher enu_pub;
		ROAMestimation::FactorGraphFilter* filter;
		//ImuHandler* imuHandler;

	private:
		ros::Subscriber imu_sub;
		ros::Subscriber encoder_sub;
		ros::Subscriber mag_sub;
		ros::Subscriber odom_sub;
		ros::Subscriber gps_sub;
		ros::Subscriber sub;
		ros::Subscriber init_sub;
		bool init_done;
		// timestamp of initial pose
		double t0;


		tf::TransformBroadcaster tf_broadcaster;

};

}

#endif /* OdomImuGps_RaROAM_H_ */
