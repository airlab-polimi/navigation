/*
 * ImuHandler.h
 *
 *  Created on: 11 apr 2017
 *      Author: venu
 */

#ifndef INCLUDE_IMUHANDLER_H_
#define INCLUDE_IMUHANDLER_H_

#include <ROAMestimation/ROAMestimation.h>
#include <ROAMimu/IMUIntegralHandler.h>

namespace raroamfree
{
class ImuHandler
{
public:
	ImuHandler(ROAMestimation::FactorGraphFilter* filter, int imu_N, double imu_dt, bool isAccBiasFixed,
				bool isGyroBiasFixed);
	void addMeasurement(double za[3], double zw[3], double t);

	inline void setBias(const Eigen::VectorXd& accBias,
				const Eigen::VectorXd& gyroBias)
	{
	  assert(!initialized); // if it is already initialized we can no longer set the biases

		this->accBias = accBias;
		this->gyroBias = gyroBias;
	}

	inline void setSensorframe(const Eigen::VectorXd& T_OS_IMU,
				const Eigen::VectorXd& x0)
	{
		this->T_OS_IMU = T_OS_IMU;
		this->x0 = x0;
	}

	inline double getPoseRate() const
	{
		return 1.0 / (imu_N * imu_dt);
	}

private:
	void initialize(double t);

private:
	ROAMestimation::FactorGraphFilter* filter;
	ROAMimu::IMUIntegralHandler* imu;
	bool initialized;

	bool isAccBiasFixed;
	Eigen::VectorXd accBias;

	bool isGyroBiasFixed;
	Eigen::VectorXd gyroBias;

	Eigen::VectorXd T_OS_IMU;
	Eigen::VectorXd x0;

	int imu_N;
	double imu_dt;

};

}




#endif /* INCLUDE_IMUHANDLER_H_ */
