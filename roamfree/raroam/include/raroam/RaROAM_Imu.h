/*
 * c_slam_roamfree,
 *
 *
 * Copyright (C) 2014 Davide Tateo
 * Versione 1.0
 *
 * This file is part of c_slam_roamfree.
 *
 * c_slam_roamfree is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * c_slam_roamfree is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with c_slam_roamfree.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef RAROAM_IMU_H_
#define RAROAM_IMU_H_

#include <ros/ros.h>
#include <sensor_msgs/Imu.h>
#include <geometry_msgs/Vector3.h>
#include <nav_msgs/Odometry.h>
#include <tf/tf.h>
#include <tf/transform_broadcaster.h>

#include <string>

#include "raroam/RaROAM_Config.h"
#include "raroam/ImuHandler.h"

namespace raroamfree
{

class RaROAM_Imu
{
	public:
		RaROAM_Imu(RaROAM_Config& config);
		~RaROAM_Imu();
		void run();


	protected:
		void imuCb(const sensor_msgs::Imu &msg);
		void imuCbVec(const geometry_msgs::Vector3 &msg);
		void odomCb(const nav_msgs::Odometry& msg);
		void publishPose();

	private:
		void initIMU();
		void initRoamfree();

	protected:
		RaROAM_Config& config;

	protected:
		ros::NodeHandle n;
		//ros::Publisher markers_pub;
		ROAMestimation::FactorGraphFilter* filter;
		ImuHandler* imuHandler;

	private:
		ros::Subscriber imu_sub;
		ros::Subscriber imu_vec_sub;
		ros::Subscriber odom_sub;
		tf::TransformBroadcaster br;

};

}

#endif /* RAROAM_IMU_H_ */
