/*
 * c_slam_roamfree,
 *
 *
 * Copyright (C) 2014 Davide Tateo
 * Versione 1.0
 *
 * This file is part of c_slam_roamfree.
 *
 * c_slam_roamfree is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * c_slam_roamfree is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with c_slam_roamfree.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef RAROAMCONFIG_H_
#define RAROAMCONFIG_H_

#include <string>
#include <stdexcept>
#include <Eigen/Dense>
#include <ros/ros.h>
#include <tf/transform_listener.h>

namespace raroamfree
{
class RaROAM_Config
{
public:
	RaROAM_Config();

	//Ros stuff
	std::string trackedFrame;

	//RobotParameters

	//Initial Pose
	bool getInitialPoseByTopic;
	Eigen::VectorXd x0;

	// tranformations trackedFrame->sensorFrames
	struct enabledSensors_t {
		bool gps;
		bool accelerometer;
		bool magnetometer;
		bool gyroscope;
	} enabledSensors;

	struct sensorsTransformation_t {
		Eigen::VectorXd gps;
		Eigen::VectorXd imu;
		Eigen::VectorXd odom;
	} sensorsTransformation;
	
	struct magnetometerParam_t {
		Eigen::VectorXd h0;
		Eigen::VectorXd r0;
		Eigen::VectorXd s0;
	} magnetometerParam;

	struct odomParam_t {
		double wheels_radius;
		double wheels_separation;
		double wheel_separation_multiplier;
	} odomParam;




	//Imu bias
	bool isAccBiasFixed;
	Eigen::VectorXd accBias;
	bool isGyroBiasFixed;
	Eigen::VectorXd gyroBias;

	//Imu data
	int imu_N;
	double imu_dt;

	//Noise generation data
	double gyro_stdev, acc_stdev;

	Eigen::VectorXd gyro_generated_bias, acc_generated_bias;

	//Integration window lenght
	double poseWindowLength;

	//Optimization method
	bool useGaussNewton;
	int iterationN;

private:
	ros::NodeHandle n;
	tf::TransformListener listener;
	void initROS();
	void initRoamfree();
	void initIMU();
	void initGPS();
	void initOdom();

	// name of frames associated to sensord
	struct sensorFrames_t {
		std::string gps;
		std::string odom;
		std::string imu;
	} sensorFrames;
};

}


#endif /* RAROAMCONFIG_H_ */
