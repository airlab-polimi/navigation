
#include "ros/ros.h"

#define NAME_OF_THIS_NODE "vec3_2_magfield"

#include "sensor_msgs/MagneticField.h"
#include "geometry_msgs/Vector3Stamped.h"

//-----------------------------------------------------------------
//-----------------------------------------------------------------

class ROSnode
{
  private: 
    ros::NodeHandle Handle;
    // publisher and subscribers
    ros::Subscriber Vector3Subscriber;
    ros::Publisher MagneticFieldPublisher;
    // callbacks
    void Vector3Callback(const geometry_msgs::Vector3Stamped::ConstPtr& msg);
    sensor_msgs::MagneticField output_msg;

  public:
    void Prepare(void);
    void RunContinuously(void);
};

//-----------------------------------------------------------------
//-----------------------------------------------------------------

void ROSnode::Prepare(void)
{
  MagneticFieldPublisher = Handle.advertise<sensor_msgs::MagneticField>("magfield_topic", 100);
  Vector3Subscriber = Handle.subscribe("vec3_topic", 10, &ROSnode::Vector3Callback, this);
  ROS_INFO("Node %s ready to run.", ros::this_node::getName().c_str());
}


void ROSnode::RunContinuously(void)
{
  ROS_INFO("Node %s running continuously.", ros::this_node::getName().c_str());
  ros::spin();
}

void ROSnode::Vector3Callback(const geometry_msgs::Vector3Stamped::ConstPtr& msg)
{
  // magnetic field direction
  output_msg.magnetic_field = msg->vector;
  // covariance
  output_msg.magnetic_field_covariance[0] = 1;
  output_msg.magnetic_field_covariance[4] = 1;
  output_msg.magnetic_field_covariance[8] = 1;
  MagneticFieldPublisher.publish(output_msg);
}


//-----------------------------------------------------------------
//-----------------------------------------------------------------


int main(int argc, char **argv)
{
  ros::init(argc, argv, NAME_OF_THIS_NODE);
  ROSnode MyNode;
   
  MyNode.Prepare();
  MyNode.RunContinuously();
  return (0);
}


/*************************************************************
 * NOTE. If you want this file to be compiled and the
 * corresponding executable to be generated, remember to add a
 * suitable rosbuild_add_executable line to the CMakeLists.txt
 * file of your package. [Warning: the contents of this note is
 * valid for versions of ROS that use the (older) rosbuild
 * build system. They may be obsolete if your version of ROS is,
 * instead, based on the newer catkin build system.]
 *************************************************************/