 # roslaunch grape husky_vine.launch:
sudo apt-get install ros-kinetic-map-server -y &&
sudo apt-get install ros-kinetic-amcl -y &&
sudo apt-get install ros-kinetic-lms1xx -y &&
sudo apt-get install ros-kinetic-controller-manager -y &&
sudo apt-get install ros-kinetic-interactive-marker-twist-server -y &&
sudo apt-get install ros-kinetic-twist-mux -y &&
sudo apt-get install ros-kinetic-gazebo-ros-control -y &&
sudo apt-get install ros-kinetic-hector-gazebo-plugins -y &&
sudo apt-get install ros-kinetic-joint-trajectory-controller -y &&
sudo apt-get install ros-kinetic-joint-state-controller -y &&
sudo apt-get install ros-kinetic-diff-drive-controller -y &&
sudo apt-get install ros-kinetic-message-to-tf -y &&                 
 # roslaunch husky_viz view_robot.launch
sudo apt-get install ros-kinetic-rviz-imu-plugin -y &&
 # roslaunch husky_vine_gmapping.launch
sudo apt-get install ros-kinetic-gmapping -y &&
 # roslaunch husky_control teleop.launch
sudo apt-get install ros-kinetic-joy -y &&
sudo apt-get install ros-kinetic-teleop-twist-joy -y &&
 # miscellaneous
sudo apt-get install ros-kinetic-ur-description -y &&
sudo apt-get install ros-kinetic-dwa-local-planner -y &&
sudo apt-get install ros-kinetic-frontier-exploration -y
