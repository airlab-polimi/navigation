Husky files for GRAPE project
===================


This folder contains all necessary files to run and simulate Husky robot in vineyard. 

System requirements
-------------------
- Ubuntu 14.04
- ROS Indigo
- Gazebo 7

Enviroment description
----------------------
The simulation enviroment consist of the Husky robot, the rough terrain and two rows of vines. This configuration might be very huge for normal pc's because the simulation part requires a too much computation power. A flat terrain can be used insted of irregular one. (see ./grape/worlds/grape.world where both are available). The vine models are also quite big. The colision area is now the same as the geometry, but can be downsampled if is needed.


Folder structure
----------------

Folder			| Description
-------			| -----------
./grape			| Contains the main launchfile and the world configuration
./husky 		| All Husky configuration and description files
./husky_simulator	| All nedeed files for Husky simulation
./velodyne_simulator	| All needed files for velodyne sensor
./ToBeCopied... | This folder contains the ground and vine models. Both folders inside must be copied into ~/.gazebo/models 

Usage
-----
- Add the grape workspace in your .bashrc file:
> source /path/to/your/workspace/devel/setup.bash --extend

-  Compile the project. 
> catkin_make

- Execution on Gazebo
> roslaunch grape husky_vine.lunch
