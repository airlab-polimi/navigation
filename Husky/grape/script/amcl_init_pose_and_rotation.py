#!/usr/bin/env python

# The script publishes the initial position for amcl algorithm, averaging on a few GPS position and IMU orientation
# It requires the robot to stay still for about 20 seconds

import rospy
from nav_msgs.msg      import Odometry
from geometry_msgs.msg import PoseWithCovarianceStamped
from sensor_msgs.msg   import Imu


		######################################
		#############  GLOBALS   #############
		######################################


pos_x_list  = []
pos_y_list  = []
rot_x_list  = []
rot_y_list  = []
rot_z_list  = []
rot_w_list  = []

pos_x_avg = 0
pos_y_avg = 0
rot_x_avg = 0
rot_y_avg = 0
rot_z_avg = 0
rot_w_avg = 0

gps_subscriber = None
gps_callback_counter = 0

imu_subscriber = None
imu_callback_counter = 0

# covariance matrix is filled with default values from amcl 
covariance = [0.25, 0.0,  0.0, 0.0,  0.0,  0.0,
              0.0,  0.25, 0.0, 0.0,  0.0,  0.0,
              0.0,  0.0,  0.0, 0.0,  0.0,  0.0,
              0.0,  0.0,  0.0, 0.0,  0.0,  0.0,
              0.0,  0.0,  0.0, 0.0,  0.0,  0.0,
              0.0,  0.0,  0.0, 0.0,  0.0,  0.06]
              # 0.0,  0.0,  0.0, 0.0,  0.0,  0.12]


		######################################
		###########   FUNCTIONS   ############
		######################################


def imu_callback(data):
	global rot_x_list
	global rot_y_list
	global rot_z_list
	global rot_w_list
	global rot_x_avg
	global rot_y_avg
	global rot_z_avg
	global rot_w_avg

	global imu_callback_counter

	rot_x_list.append(data.orientation.x)
	rot_y_list.append(data.orientation.y)
	rot_z_list.append(data.orientation.z)
	rot_w_list.append(data.orientation.w)
	imu_callback_counter = imu_callback_counter+1
	# if I have enough messages, unsubscribe to this topic
	# the gps callback (much slower) will determine the end of the averaging phase
	if imu_callback_counter >= 20 :
		global imu_subscriber
		imu_subscriber.unregister()

		rot_x_avg = sum(rot_x_list)/imu_callback_counter 
		rot_y_avg = sum(rot_y_list)/imu_callback_counter 
		rot_z_avg = sum(rot_z_list)/imu_callback_counter
		rot_w_avg = sum(rot_w_list)/imu_callback_counter


def gps_callback(data):
	global pos_x_list
	global pos_y_list
	global pos_x_avg
	global pos_y_avg

	global gps_callback_counter


	pos_x_list.append(data.pose.pose.position.x)
	pos_y_list.append(data.pose.pose.position.y)
	gps_callback_counter = gps_callback_counter+1
	# if I have enough messages, unsubscribe to this topic, then create the initial pose and publish it
	if gps_callback_counter >= 10 :
		global gps_subscriber
		gps_subscriber.unregister()

		pos_x_avg = sum(pos_x_list)/gps_callback_counter
		pos_y_avg = sum(pos_y_list)/gps_callback_counter
		send_init_pose()


def send_init_pose():
	initial_pose_pub = rospy.Publisher('/initialpose', PoseWithCovarianceStamped, queue_size=10, latch = True)
	msg = PoseWithCovarianceStamped()
	msg.header.seq = 1
	msg.header.frame_id = 'map'
	msg.pose.covariance = covariance
	msg.pose.pose.position.x = pos_x_avg
	msg.pose.pose.position.y = pos_y_avg
	msg.pose.pose.position.z = 0;
	msg.pose.pose.orientation.x = rot_x_avg
	msg.pose.pose.orientation.y = rot_y_avg
	msg.pose.pose.orientation.z = rot_z_avg
	msg.pose.pose.orientation.w = rot_w_avg
	initial_pose_pub.publish(msg);
	print "AMCL initial pose published!"




if __name__ == '__main__':
	rospy.init_node('amcl_initial_state_publisher', anonymous=True)
	gps_subscriber = rospy.Subscriber("/odometry/gps", Odometry, gps_callback)
	imu_subscriber = rospy.Subscriber("/imu/data", Imu, imu_callback)
	rospy.spin();



