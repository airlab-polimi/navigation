//This node convert gps fix from LLH to ENU
//It is possible to define the reference point as a parameter
//Topic published: /enu
//Message: geometry_msgs/PoseStamped
//Topic subscribed: /fix
//Message: sensor_msgs/NavSatFix
//Parameters: latitude, longitude, height - reference point
#include <cmath>
#include <ros/ros.h>
#include <sensor_msgs/NavSatFix.h>
#include <geometry_msgs/PoseWithCovarianceStamped.h>
#include "gps_converter/Llh_to_enu.h"

#define NO_FIX -1
#define COVARIANCE_TYPE_UNKNOWN 0

#define F 1.0/298.257223563
#define A 6378137
#define E2 F*(2 - F)

 double X=0.0, Y=0.0, Z=0.0;
 double senphi=0.0, cosphi=0.0;
 double senlambda=0.0, coslambda=0.0; 
 double lat=0.0, lon=0.0, h=0.0;

bool init() {

  //some math, used multiple times
  senphi = sin(lat * M_PI / 180.0);
  cosphi = cos(lat * M_PI / 180.0);
  senlambda = sin(lon * M_PI / 180.0);
  coslambda = cos(lon * M_PI / 180.0);

  double v = A / sqrt(1 - E2 * senphi * senphi);
  
  // reference coordinates in ECEF
  X = (v + h) * cosphi * coslambda;
  Y = (v + h) * cosphi * senlambda;
  Z = v*(1 - E2) * senphi;
 
  
  ROS_INFO("Node %s ready to run.", ros::this_node::getName().c_str());
  
  return true;
}

bool llh_to_enu(gps_converter::Llh_to_enu::Request &req,
				gps_converter::Llh_to_enu::Response &res)
{
  double x, y, z;
  double clat, slat, clon, slon;

  slat = sin(req.latitude * M_PI / 180.0);
  clat = cos(req.latitude * M_PI / 180.0);
  slon = sin(req.longitude * M_PI / 180.0);
  clon = cos(req.longitude * M_PI / 180.0);
  
  double v = A / sqrt(1 - E2 * slat * slat);

  // fix in ECEF coordinates
  x = (v + req.altitude) * clat * clon;
  y = (v + req.altitude) * clat * slon;
  z = v*(1 - E2) * slat;

  geometry_msgs::PoseWithCovarianceStamped enu;

  //from ECEF to ENU
  res.x = -senlambda * (x-X) + coslambda * (y-Y);
  res.y = -senphi * coslambda * (x-X) - senphi * senlambda * (y-Y) + cosphi * (z-Z);
  res.z = cosphi * coslambda * (x-X) + cosphi * senlambda * (y-Y) + senphi * (z-Z);
  
  return true;
}

int main(int argc, char **argv) {
  ros::init(argc, argv, "llh_to_enu_service");
  ros::NodeHandle handle;
  
   //Retrieve parameters
  if (handle.getParam("/llh_to_enu_service/latitude", lat)) {
    ROS_INFO("Node %s: retrieved parameter latitude.", ros::this_node::getName().c_str());
  }
  else {
    ROS_FATAL("Node %s: unable to retrieve parameter latitude.", ros::this_node::getName().c_str());
    return false;
  }
  if (handle.getParam("/llh_to_enu_service/longitude", lon)) {
    ROS_INFO("Node %s: retrieved parameter longitude.", ros::this_node::getName().c_str());
  }
  else {
    ROS_FATAL("Node %s: unable to retrieve parameter longitude.", ros::this_node::getName().c_str());
    return false;
  }
  if (handle.getParam("/llh_to_enu_service/height", h)) {
    ROS_INFO("Node %s: retrieved parameter height.", ros::this_node::getName().c_str());
  }
  else {
    ROS_FATAL("Node %s: unable to retrieve parameter height.", ros::this_node::getName().c_str());
    return false;
  }
  
  
  if(!init())
    return 1;
    
  ros::ServiceServer service = handle.advertiseService("llh_to_enu", llh_to_enu);
  
  ros::spin(); 
  
  return (0);
}
