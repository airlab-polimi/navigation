//This node convert gps fix from LLH to ENU
//It is possible to define the reference point as a parameter
//Topic published: /enu
//Message: geometry_msgs/PoseStamped
//Topic subscribed: /fix
//Message: sensor_msgs/NavSatFix
//Parameters: latitude, longitude, height - reference point
#include <cmath>
#include <ros/ros.h>
#include <sensor_msgs/NavSatFix.h>
#include <geometry_msgs/PoseWithCovarianceStamped.h>
#include "gps_converter/Llh_to_enu.h"

#define NO_FIX -1
#define COVARIANCE_TYPE_UNKNOWN 0
double m_reflat=0.0, m_reflon=0.0, h=0.0;
double delta_Lon_per_pixel=0.0, delta_Lat_per_pixel=0.0;
double m_refx = 0;
double m_refy = 0;
double m_pixel_res = 25.0;

bool llh_to_enu(gps_converter::Llh_to_enu::Request &req,
				gps_converter::Llh_to_enu::Response &res)
{

	double radLat_ref = (m_reflat * M_PI) / 180.0;
	double radLat = (req.latitude * M_PI) / 180.0;
	//p.X = p.X * (Math.Cos(radLat_ref)/Math.Cos(radLat) );

	// Cos(radLat) = cos(lat_ref) - delta_lat * sin(lat_ref)
	double cos_radlat = cos(radLat_ref) - (m_reflat - req.latitude) * sin(radLat_ref);

	// delta_lon(reprojection) = delta_lon * ( cos(lat_ref) / cos(lat) )
	double delta_lon_proj = (req.longitude - m_reflon) * (cos(radLat_ref) / cos_radlat);


	double delta_pixel_lat = (m_reflat - req.latitude) / delta_Lat_per_pixel;
	// double delta_pixel_lon = (p.X - m_reflon ) / delta_Lon_per_pixel;
	double delta_pixel_lon = delta_lon_proj / delta_Lon_per_pixel;


	res.x = delta_pixel_lon*m_pixel_res/100;
	res.y = -delta_pixel_lat*m_pixel_res/100;
  
  return true;
}

int main(int argc, char **argv) {
  ros::init(argc, argv, "llh_to_enu_service");
  ros::NodeHandle handle;
  
   //Retrieve parameters
  if (handle.getParam("/llh_to_enu_service/latitude", m_reflat)) {
    ROS_INFO("Node %s: retrieved parameter latitude.", ros::this_node::getName().c_str());
  }
  else {
    ROS_FATAL("Node %s: unable to retrieve parameter latitude.", ros::this_node::getName().c_str());
    return false;
  }
  if (handle.getParam("/llh_to_enu_service/longitude", m_reflon)) {
    ROS_INFO("Node %s: retrieved parameter longitude.", ros::this_node::getName().c_str());
  }
  else {
    ROS_FATAL("Node %s: unable to retrieve parameter longitude.", ros::this_node::getName().c_str());
    return false;
  }
  if (handle.getParam("/llh_to_enu_service/height", h)) {
    ROS_INFO("Node %s: retrieved parameter height.", ros::this_node::getName().c_str());
  }
  else {
    ROS_FATAL("Node %s: unable to retrieve parameter height.", ros::this_node::getName().c_str());
    return false;
  }
  
  delta_Lon_per_pixel = ((m_pixel_res / 100.0) / (1852.0 * 60)) / cos((m_reflat * M_PI) / 180.0);
  delta_Lat_per_pixel = ((m_pixel_res / 100) / (1852 * 60));
  
  ros::ServiceServer service = handle.advertiseService("llh_to_enu", llh_to_enu);
  
  ros::spin(); 
  
  return (0);
}
