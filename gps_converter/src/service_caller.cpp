#include "ros/ros.h"
#include "std_msgs/String.h"
#include "gps_converter/Llh_to_enu.h"
#include <sensor_msgs/NavSatFix.h>
#include <geometry_msgs/PoseWithCovarianceStamped.h>


ros::Publisher pub;
ros::Subscriber sub;
ros::ServiceClient client; 

/**
 * This tutorial demonstrates simple receipt of messages over the ROS system.
 */
void latlonCallback(const sensor_msgs::NavSatFix::ConstPtr& fix)
{
  gps_converter::Llh_to_enu srv;
  srv.request.latitude  = fix->latitude;
  srv.request.longitude = fix->longitude;
  if(client.call(srv)){
    geometry_msgs::PoseWithCovarianceStamped enu;
    enu.pose.pose.position.x = srv.response.x;
    enu.pose.pose.position.y = srv.response.y;
    pub.publish(enu);
  } else
    ROS_ERROR("Na");


}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "listener");
  ros::NodeHandle n;
  client = n.serviceClient<gps_converter::Llh_to_enu>("llh_to_enu");
  sub = n.subscribe("/latlon", 1000, latlonCallback);
  pub = n.advertise<geometry_msgs::PoseWithCovarianceStamped>("enu", 10);
  ros::spin();

  return 0;
}